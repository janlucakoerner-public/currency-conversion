package de.janlucakoerner;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Calendar;
import java.util.Objects;

import de.janlucakoerner.Constants.Sort;
import de.janlucakoerner.Constants.Column;

/**
 * This class requests the exchange rates for all currencies to EUR via HTTP.
 * The exchange rates are delivered in XML format.
 *
 * @author janlucakoerner
 * @version 1.0
 * @since 2022.12.21
 * @see <a href="https://www.zoll.de/DE/Fachthemen/Zoelle/Zollwert/Aktuelle-Wechselkurse/Datenbankanwendung/abruf_wechselkurse.html?nn=298574&faqCalledDoc=298574">Zoll exchange rates</a>
 */
public class Requester {

    /**
     * This array contains the url snippets which are split up at the variable parts of the url.
     * The variable parts will be inserted in between of the array values.
     */
    private static final String[] URL_BASE = {
            "https://www.zoll.de/SiteGlobals/Functions/Kurse/KursExport.",
            "?view=xmlexportkursesearchresultZOLLWeb&kursart=",
            "&iso2code2=",
            "&startdatum_tag2=",
            "&startdatum_monat2=",
            "&startdatum_jahr2=",
            "&enddatum_tag2=",
            "&enddatum_monat2=",
            "&enddatum_jahr2=",
            "&sort=",
            "&spalte="
    };

    /**
     * This method builds the url by adding the arguments in between of the url base. All arguments are required.
     * @param fileFormat The format of the requested file. Possible choices are FileFormat.XML or FileFormat.CSV.
     * @param exchangeRateType The exchangeRateType. Possible choices are ExchangeRateType.NOTED_CURRENCY,
     *                         ExchangeRateType.NOT_NOTED_CURRENCY or ExchangeRateType.IATA_EXCHANGE_RATE
     * @param country
     * @param from
     * @param until
     * @param sort
     * @param column
     * @return The HTTP request url
     */
    public static String buildUrl(String fileFormat, int exchangeRateType, String country, Calendar from,
                                  Calendar until, String sort, String column) {
        String url = "";
        if (exchangeRateType >= 0 && exchangeRateType <= 3) {
            url += URL_BASE[0];
            url += fileFormat;
            url += URL_BASE[1];
            url += Integer.toString(exchangeRateType);
            if (!Objects.equals(country, "") && country != null) {
                url += URL_BASE[2];
                url += country;
            }
            url += URL_BASE[3];
            url += String.format("%02d", from.get(Calendar.DATE));
            url += URL_BASE[4];
            url += String.format("%02d", from.get(Calendar.MONTH) + 1);
            url += URL_BASE[5];
            url += String.format("%04d", from.get(Calendar.YEAR));
            url += URL_BASE[6];
            url += String.format("%02d", until.get(Calendar.DATE));
            url += URL_BASE[7];
            url += String.format("%02d", until.get(Calendar.MONTH) + 1);
            url += URL_BASE[8];
            url += String.format("%04d", until.get(Calendar.YEAR));
            url += URL_BASE[9];
            switch (sort) {
                case Sort.ASC, Sort.DESC -> url += sort;
                default -> url += Sort.ASC;
            }
            url += URL_BASE[10];
            switch (column) {
                case Column.COUNTRY, Column.VALIDITY -> url += column;
                default -> url += Column.VALIDITY;
            }
        }
        return url;
    }
    public static String buildUrl(String fileFormat, int exchangeRateType, Calendar from,
                                  Calendar until, String sort, String column) {
        return buildUrl(fileFormat, exchangeRateType, null, from, until, sort, column);
    }
    public static String buildUrl(String fileFormat, int exchangeRateType, Calendar from,
                                  Calendar until) {
        return buildUrl(fileFormat, exchangeRateType, null, from, until, Sort.ASC, Column.VALIDITY);
    }
    public static String buildUrl(String fileFormat, int exchangeRateType) {
        return buildUrl(fileFormat, exchangeRateType,null, Calendar.getInstance(), Calendar.getInstance(), Sort.ASC, Column.VALIDITY);
    }
    private static String formatDate(Calendar date) {
        return String.format("%04d", date.get(Calendar.YEAR)) +
                String.format("%02d", date.get(Calendar.MONTH) + 1) +
                String.format("%02d", date.get(Calendar.DATE)) +
                String.format("%02d", date.get(Calendar.HOUR)) +
                String.format("%02d", date.get(Calendar.MINUTE)) +
                String.format("%02d", date.get(Calendar.SECOND)) +
                String.format("%03d", date.get(Calendar.MILLISECOND));
    }
    public static String getExchangeRates(String urlPath, String fileFormat) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder(URI.create(urlPath))
                .header("Accept", "application/" + fileFormat)
                .GET()
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }
}
