package de.janlucakoerner.Constants;

public abstract class FileFormat {
    public static final String XML = "xml";
    public static final String CSV = "csv";
}
