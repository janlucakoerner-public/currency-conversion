package de.janlucakoerner.Constants;

public abstract class ExchangeRateType {
    public static final int NOTED_CURRENCY = 1;
    public static final int NOT_NOTED_CURRENCY = 2;
    public static final int IATA_EXCHANGE_RATE = 3;
}
