package de.janlucakoerner.Constants;

public class Sort {
    public static final String ASC = "asc";
    public static final String DESC = "desc";
    public static final String ASCENDING = ASC;
    public static final String DESCENDING = DESC;
}
