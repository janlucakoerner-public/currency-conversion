package de.janlucakoerner;

import de.janlucakoerner.Constants.ExchangeRateType;
import de.janlucakoerner.Constants.FileFormat;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        String url = Requester.buildUrl(FileFormat.XML, ExchangeRateType.IATA_EXCHANGE_RATE);
        System.out.println(url);
        try {
            String data = Requester.getExchangeRates(url, FileFormat.XML);
            System.out.println(data);
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}